package com.epam.restaurant.strategy.domain;

import com.epam.restaurant.utils.Happiness;

public interface EffectApplyStrategy {

    public void apply(Happiness happiness, double ammount);
}
