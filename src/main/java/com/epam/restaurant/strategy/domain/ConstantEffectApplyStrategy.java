package com.epam.restaurant.strategy.domain;

import org.springframework.stereotype.Component;

import com.epam.restaurant.utils.Happiness;

@Component
public class ConstantEffectApplyStrategy implements EffectApplyStrategy {

    @Override
    public void apply(Happiness happiness, double ammount) {
        double currentHappiness = happiness.getHappiness();
        double newHappiness = currentHappiness + ammount;
        happiness.setHappiness(newHappiness);
    }

}
