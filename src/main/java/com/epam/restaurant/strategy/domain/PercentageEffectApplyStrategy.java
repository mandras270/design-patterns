package com.epam.restaurant.strategy.domain;

import org.springframework.stereotype.Component;

import com.epam.restaurant.utils.Happiness;

@Component
public class PercentageEffectApplyStrategy implements EffectApplyStrategy {

    @Override
    public void apply(Happiness happiness, double ammount) {
        double currentHappiness = happiness.getHappiness();
        double newHappiness = currentHappiness * multiplier(ammount) + currentHappiness;
        happiness.setHappiness(newHappiness);
    }

    private double multiplier(double ammount) {
        return ammount / 100;
    }

}
