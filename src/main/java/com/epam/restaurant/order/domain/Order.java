package com.epam.restaurant.order.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.restaurant.client.domain.Client;
import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.product.domain.Product;

@Component
@Scope("prototype")
public class Order {

    private Product product;
    private List<Extra> extras;
    private Client client;

    public Order() {
        super();
        extras = new ArrayList<>();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Extra> getExtras() {
        return extras;
    }

    public void addExtra(Extra extra) {
        extras.add(extra);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
