package com.epam.restaurant.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.restaurant.client.domain.Client;
import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.extra.domain.Ketchup;
import com.epam.restaurant.extra.domain.Mustard;
import com.epam.restaurant.order.domain.Order;
import com.epam.restaurant.product.domain.Chips;
import com.epam.restaurant.product.domain.HotDog;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.robot.domain.Robot;

public class App {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.epam.restaurant");

        Robot robot = context.getBean("robot", Robot.class);
        Client client1 = context.getBean("client", Client.class);

        Product hotDog = context.getBean("hotDog", HotDog.class);
        Product chips = context.getBean("chips", Chips.class);

        Extra ketchup = context.getBean("ketchup", Ketchup.class);
        Mustard mustard = context.getBean("mustard", Mustard.class);

        Order order = context.getBean("order", Order.class);

        order.setClient(client1);
        order.setProduct(hotDog);
        order.addExtra(ketchup);
        order.addExtra(mustard);

        client1.placeOrder(order);

        robot.work();

        context.close();

    }
}
