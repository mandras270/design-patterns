package com.epam.restaurant.client.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.restaurant.mediator.domain.Mediator;
import com.epam.restaurant.order.domain.Order;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.utils.Happiness;

@Component
@Scope("prototype")
public class Client {

    @Autowired
    private Happiness happiness;

    @Autowired
    private Mediator mediator;

    public void setHappiness(Happiness happiness) {
        this.happiness = happiness;
    }

    public double getHappinesslevel() {
        return happiness.getHappiness();
    }

    public void placeOrder(Order order) {
        mediator.order(order, this);
    }

    public void consume(Product product) {
        product.applyEffect(happiness);
    }

}
