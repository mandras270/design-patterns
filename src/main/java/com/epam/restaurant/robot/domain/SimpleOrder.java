package com.epam.restaurant.robot.domain;

import java.util.List;

import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.product.domain.Product;

public class SimpleOrder {

    private Product product;
    private List<Extra> extras;

    public SimpleOrder(Product product, List<Extra> extras) {
        super();
        this.product = product;
        this.extras = extras;
    }

    public Product getProduct() {
        return product;
    }

    public List<Extra> getExtras() {
        return extras;
    }
}
