package com.epam.restaurant.robot.domain;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.mediator.domain.Mediator;
import com.epam.restaurant.product.domain.Product;

@Component
public class Robot {

    private Mediator mediator;
    private Queue<SimpleOrder> productQueue;

    public Robot() {
        super();
        productQueue = new ConcurrentLinkedDeque<>();
    }

    @Autowired
    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    public void work() {
        SimpleOrder order;

        while ((order = productQueue.poll()) != null) {

            List<Extra> extras = order.getExtras();
            Product product = order.getProduct();

            addExtras(extras, product);
            reportReadyProduct(product);
        }
    }

    private void reportReadyProduct(Product product) {
        mediator.productIsReady(product);
    }

    private void addExtras(List<Extra> extras, Product product) {
        for (Extra extra : extras) {
            extra.apply(product);
        }
    }

    public int getNumberOfProductsInQueue() {
        return productQueue.size();
    }

    public void createProduct(SimpleOrder simpleOrder) {
        productQueue.add(simpleOrder);
    }
}
