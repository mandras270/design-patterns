package com.epam.restaurant.robot.transformer;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.robot.domain.SimpleOrder;

@Component
public class SimpleOrderTransformer {

    public SimpleOrder transformSimpleOrder(Product product, List<Extra> extras) {
        SimpleOrder result = new SimpleOrder(product, extras);
        return result;
    }

}
