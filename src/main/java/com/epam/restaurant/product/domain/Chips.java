package com.epam.restaurant.product.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.restaurant.strategy.domain.EffectApplyStrategy;
import com.epam.restaurant.utils.Happiness;

@Component
public class Chips extends Product {

    private static final double BASE_CHIPS_HAPPISESS = 5.0;

    public Chips() {
        super(BASE_CHIPS_HAPPISESS);
    }

    @Override
    public void applyEffect(Happiness happiness) {
        effectApplyStrategy.apply(happiness, getEffectAmmount());
    }

    @Autowired
    @Qualifier("percentageEffectApplyStrategy")
    @Override
    public void setEffectApplyStrategy(EffectApplyStrategy effectApplyStrategy) {
        this.effectApplyStrategy = effectApplyStrategy;
    }

}
