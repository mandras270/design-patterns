package com.epam.restaurant.product.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.epam.restaurant.strategy.domain.EffectApplyStrategy;
import com.epam.restaurant.utils.Happiness;

@Component
public class HotDog extends Product {

    private static final double BASE_HOTDOG_HAPPINESS = 2.0;

    public HotDog() {
        super(BASE_HOTDOG_HAPPINESS);
    }

    @Override
    public void applyEffect(Happiness happiness) {
        effectApplyStrategy.apply(happiness, getEffectAmmount());
    }

    @Autowired
    @Qualifier("constantEffectApplyStrategy")
    @Override
    public void setEffectApplyStrategy(EffectApplyStrategy effectApplyStrategy) {
        this.effectApplyStrategy = effectApplyStrategy;
    }
}
