package com.epam.restaurant.product.domain;

import com.epam.restaurant.effect.domain.Effect;
import com.epam.restaurant.effect.domain.ProductEffect;
import com.epam.restaurant.strategy.domain.EffectApplyStrategy;
import com.epam.restaurant.utils.Happiness;

public abstract class Product {

    private Effect baseEffect;
    protected EffectApplyStrategy effectApplyStrategy;

    public Product(double baseEffectAmmount) {
        super();
        this.baseEffect = new ProductEffect(baseEffectAmmount);
    }

    public double getEffectAmmount() {
        return baseEffect.getAmmount();
    }

    public void setEffectAmmount(double newAmmount) {
        baseEffect.setAmmount(newAmmount);
    }

    public Effect getBaseEffect() {
        return baseEffect;
    }

    public abstract void applyEffect(Happiness happiness);

    public abstract void setEffectApplyStrategy(EffectApplyStrategy effectApplyStrategy);
}
