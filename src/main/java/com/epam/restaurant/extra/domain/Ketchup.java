package com.epam.restaurant.extra.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.restaurant.product.domain.Product;

@Component
@Scope("prototype")
public class Ketchup implements Extra {

    private static final int KETCHUP_EFFECT_MULTIPLIER = 2;

    @Override
    public void apply(Product product) {
        double ammount = product.getEffectAmmount();
        double newAmmount = ammount * KETCHUP_EFFECT_MULTIPLIER;
        product.setEffectAmmount(newAmmount);
    }
}
