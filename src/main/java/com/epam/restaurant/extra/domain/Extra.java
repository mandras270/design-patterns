package com.epam.restaurant.extra.domain;

import com.epam.restaurant.product.domain.Product;

public interface Extra {

    public void apply(Product product);

}
