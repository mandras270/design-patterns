package com.epam.restaurant.extra.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.strategy.domain.EffectApplyStrategy;

@Component
@Scope("prototype")
public class Mustard implements Extra {

    private static final int MUSTARD_EFFECT_AMMOUNT = 1;

    @Autowired
    @Qualifier("constantEffectApplyStrategy")
    private EffectApplyStrategy effectApplyStrategy;

    public void setEffectApplyStrategy(EffectApplyStrategy effectApplyStrategy) {
        this.effectApplyStrategy = effectApplyStrategy;
    }

    @Override
    public void apply(Product product) {
        product.setEffectApplyStrategy(effectApplyStrategy);
        product.setEffectAmmount(MUSTARD_EFFECT_AMMOUNT);
    }
}
