package com.epam.restaurant.mediator.domain;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.restaurant.client.domain.Client;
import com.epam.restaurant.order.domain.Order;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.robot.domain.Robot;
import com.epam.restaurant.robot.domain.SimpleOrder;
import com.epam.restaurant.robot.transformer.SimpleOrderTransformer;

@Component
public class Mediator {

    @Autowired
    private Robot robot;
    @Autowired
    private SimpleOrderTransformer simpleOrderTransformer;
    private Queue<Client> clients;

    public Mediator() {
        super();
        clients = new ConcurrentLinkedDeque<>();
    }

    public void order(Order order, Client client) {

        validate(order, client);
        addClientToList(client);
        sendOrder(order);
    }

    private void validate(Order order, Client client) {
        validateOrderIsNotNull(order);
        validateClientIsNotNull(client);
        validateProductIsNotNull(order.getProduct());
    }

    private void sendOrder(Order order) {
        SimpleOrder simpleOrder = simpleOrderTransformer.transformSimpleOrder(order.getProduct(), order.getExtras());
        robot.createProduct(simpleOrder);
    }

    private void addClientToList(Client client) {
        clients.add(client);
    }

    private void validateProductIsNotNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("The order's product cannot be null");
        }
    }

    private void validateOrderIsNotNull(Order order) {
        if (order == null) {
            throw new IllegalArgumentException("The order cannot be null");
        }
    }

    private void validateClientIsNotNull(Client client) {
        if (client == null) {
            throw new IllegalArgumentException("The order's client cannot be null");
        }
    }

    public void productIsReady(Product product) {

        Client client = clients.poll();
        client.consume(product);

    }
}
