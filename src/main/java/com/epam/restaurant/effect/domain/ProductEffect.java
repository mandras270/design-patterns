package com.epam.restaurant.effect.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ProductEffect implements Effect {

    private double ammount;

    public ProductEffect(double ammount) {
        super();
        this.ammount = ammount;
    }

    @Override
    public double getAmmount() {
        return ammount;
    }

    @Override
    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

}
