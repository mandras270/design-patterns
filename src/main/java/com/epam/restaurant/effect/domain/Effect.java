package com.epam.restaurant.effect.domain;


public interface Effect {

    public double getAmmount();

    public void setAmmount(double ammount);
}
