package com.epam.restaurant.mediator.domain;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.client.domain.Client;
import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.order.domain.Order;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.robot.domain.Robot;
import com.epam.restaurant.robot.domain.SimpleOrder;
import com.epam.restaurant.robot.transformer.SimpleOrderTransformer;

public class MediatorTest {

    @InjectMocks
    private Mediator underTest;

    @Mock
    private SimpleOrderTransformer transformer;

    @Mock
    private Robot robot;

    @Mock
    private Client client;

    @Mock
    private Client client2;

    @Mock
    private Order order;

    @Mock
    private SimpleOrder simpleOrder;

    @Mock
    private Extra ketchup;

    @Mock
    private Extra mustard;

    @Mock
    private Product product;

    @Before
    public void setUp() {
        underTest = new Mediator();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOrderCallsRobot() {
        //GIVEN
        List<Extra> extras = Arrays.asList(ketchup, mustard);
        Mockito.when(order.getProduct()).thenReturn(product);
        Mockito.when(order.getExtras()).thenReturn(extras);
        Mockito.when(transformer.transformSimpleOrder(product, extras)).thenReturn(simpleOrder);
        //WHEN
        underTest.order(order, client);
        //THEN
        InOrder inOrder = Mockito.inOrder(robot);
        inOrder.verify(robot).createProduct(simpleOrder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderOrderCannotBeNull() {
        //GIVEN
        //WHEN
        underTest.order(null, client);
        //THEN exception thrown
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderClientCannotBeNull() {
        //GIVEN
        //WHEN
        underTest.order(order, null);
        //THEN exception thrown
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrderOrdersProductCannotBeNull() {
        //GIVEN
        List<Extra> extras = Arrays.asList(ketchup, mustard);
        Mockito.when(order.getProduct()).thenReturn(null);
        Mockito.when(order.getExtras()).thenReturn(extras);
        //WHEN
        underTest.order(order, client);
        //THEN exception thrown
    }

    @Test
    public void testProductIsReady() {
        //GIVEN
        List<Extra> extras = Arrays.asList(ketchup, mustard);
        Mockito.when(order.getProduct()).thenReturn(product);
        Mockito.when(order.getExtras()).thenReturn(extras);
        underTest.order(order, client);
        underTest.order(order, client2);
        //WHEN
        underTest.productIsReady(product);
        //THEN
        InOrder inOrder = Mockito.inOrder(client, client2);
        inOrder.verify(client).consume(product);
    }
}
