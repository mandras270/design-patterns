package com.epam.restaurant.robot.domain;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.extra.domain.Extra;
import com.epam.restaurant.mediator.domain.Mediator;
import com.epam.restaurant.product.domain.Product;

public class RobotTest {

    @InjectMocks
    private Robot underTest;

    @Mock
    private Mediator mediator;

    @Mock
    private SimpleOrder simpleOrder;

    @Mock
    private Extra ketchup;

    @Mock
    private Extra mustard;

    @Mock
    private Product product;

    @Before
    public void setUp() {
        underTest = new Robot();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProductQueueIsEmptyAtStart() {
        //GIVEN
        int expectedSize = 0;
        //WHEN
        //THEN
        Assert.assertEquals(expectedSize, underTest.getNumberOfProductsInQueue());
    }

    @Test
    public void testCreateProductAddsProductToItsQueue() {
        //GIVEN
        int expectedSize = 1;
        //WHEN
        underTest.createProduct(simpleOrder);
        //THEN
        Assert.assertEquals(expectedSize, underTest.getNumberOfProductsInQueue());
    }

    @Test
    public void testWork() {
        //GIVEN
        underTest.createProduct(simpleOrder);
        List<Extra> extras = Arrays.asList(ketchup, mustard);
        Mockito.when(simpleOrder.getExtras()).thenReturn(extras);
        Mockito.when(simpleOrder.getProduct()).thenReturn(product);
        //WHEN
        underTest.work();
        //THEN
        InOrder inOrder = Mockito.inOrder(simpleOrder, ketchup, mustard, mediator);
        inOrder.verify(simpleOrder).getExtras();
        inOrder.verify(simpleOrder).getProduct();
        inOrder.verify(ketchup).apply(product);
        inOrder.verify(mustard).apply(product);
        inOrder.verify(mediator).productIsReady(product);
    }
}
