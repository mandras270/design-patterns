package com.epam.restaurant.strategy.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.utils.Happiness;

public class PercentageEffectApplyStrategyTest {

    private PercentageEffectApplyStrategy underTest;

    @Mock
    private Happiness happiness;

    @Before
    public void setUp() {
        underTest = new PercentageEffectApplyStrategy();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testApply() {
        //GIVEN
        double baseHappinessAmmount = 1.0;
        double happinessAmmountToAdd = 10.0;
        double expectedHappinessAmmount = 1.1;
        Mockito.when(happiness.getHappiness()).thenReturn(baseHappinessAmmount);
        //WHEN
        underTest.apply(happiness, happinessAmmountToAdd);
        //THEN
        InOrder inOrder = Mockito.inOrder(happiness);
        inOrder.verify(happiness).getHappiness();
        inOrder.verify(happiness).setHappiness(expectedHappinessAmmount);
    }
}
