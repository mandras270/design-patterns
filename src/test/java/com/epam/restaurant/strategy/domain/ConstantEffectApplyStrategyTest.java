package com.epam.restaurant.strategy.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.utils.Happiness;

public class ConstantEffectApplyStrategyTest {

    private ConstantEffectApplyStrategy underTest;

    @Mock
    private Happiness happiness;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        underTest = new ConstantEffectApplyStrategy();
    }

    @Test
    public void testApply() {
        //GIVEN
        double baseHappinessAmmount = 1.0;
        double happinessAmmountToAdd = 2.0;
        double expectedHappinessAmmount = 3.0;
        Mockito.when(happiness.getHappiness()).thenReturn(baseHappinessAmmount);
        //WHEN
        underTest.apply(happiness, happinessAmmountToAdd);
        //THEN
        InOrder inOrder = Mockito.inOrder(happiness);
        inOrder.verify(happiness).getHappiness();
        inOrder.verify(happiness).setHappiness(expectedHappinessAmmount);
    }

}
