package com.epam.restaurant.client.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.mediator.domain.Mediator;
import com.epam.restaurant.order.domain.Order;
import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.utils.Happiness;

public class ClientTest {

    @InjectMocks
    private Client underTest;

    @Mock
    private Mediator mediator;

    @Mock
    private Happiness happiness;

    @Mock
    private Order order;

    @Mock
    private Product product;

    @Before
    public void setUp() {
        underTest = new Client();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPlaceOrder() {
        //GIVEN
        //WHEN
        underTest.placeOrder(order);
        //THEN
        InOrder inOrder = Mockito.inOrder(mediator);
        inOrder.verify(mediator).order(order, underTest);
    }

    @Test
    public void testConsume() {
        //GIVEN
        //WHEN
        underTest.consume(product);
        //THEN
        InOrder inOrder = Mockito.inOrder(product);
        inOrder.verify(product).applyEffect(happiness);
    }
}
