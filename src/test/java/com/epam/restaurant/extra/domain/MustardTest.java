package com.epam.restaurant.extra.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.product.domain.Product;
import com.epam.restaurant.strategy.domain.ConstantEffectApplyStrategy;

public class MustardTest {

    private static final int MUSTARD_EFFECT_AMMOUNT = 1;

    @InjectMocks
    private Mustard underTest;

    @Mock
    private ConstantEffectApplyStrategy applyStrategy;

    @Mock
    private Product product;

    @Before
    public void setUp() {
        underTest = new Mustard();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testApply() {
        //GIVEN
        //WHEN
        underTest.apply(product);
        //THEN
        InOrder inOrder = Mockito.inOrder(product);
        inOrder.verify(product).setEffectApplyStrategy(applyStrategy);
        inOrder.verify(product).setEffectAmmount(MUSTARD_EFFECT_AMMOUNT);
    }
}
