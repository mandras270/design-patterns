package com.epam.restaurant.extra.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.restaurant.product.domain.Product;

public class KetchupTest {

    private static final int KETCHUP_EFFECT_MULTIPLIER = 2;

    @InjectMocks
    private Ketchup underTest;

    @Mock
    private Product product;

    @Before
    public void setUp() {
        underTest = new Ketchup();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testApply() {
        //GIVEN
        double effectAmmount = 2.0;
        double expectedAmmount = 2.0 * KETCHUP_EFFECT_MULTIPLIER;
        Mockito.when(product.getEffectAmmount()).thenReturn(effectAmmount);
        //WHEN
        underTest.apply(product);
        //THEN
        InOrder inOrder = Mockito.inOrder(product);
        inOrder.verify(product).getEffectAmmount();
        inOrder.verify(product).setEffectAmmount(expectedAmmount);
    }

}
